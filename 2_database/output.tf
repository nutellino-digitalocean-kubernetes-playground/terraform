output "database_endpoint" {
  value = "${digitalocean_database_cluster.mysql-database.host}"
}

output "database_username" {
  value = "${digitalocean_database_cluster.mysql-database.user}"
}


output "database_password" {
  value = "${digitalocean_database_cluster.mysql-database.password}"
}

output "database_port" {
  value = "${digitalocean_database_cluster.mysql-database.port}"
}