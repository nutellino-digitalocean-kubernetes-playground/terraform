provider "aws" {
  region = "${var.aws_region}"
}

provider "digitalocean" {
  token = "${var.digital_ocean_token}"
}

