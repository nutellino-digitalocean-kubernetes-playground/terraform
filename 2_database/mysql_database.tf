resource "digitalocean_database_cluster" "mysql-database" {
  name       = "${var.namespace}-mysql-cluster"
  engine     = "mysql"
  size       = "${var.do_database_size}"
  region     = "${var.do_region}"
  node_count = 1
}