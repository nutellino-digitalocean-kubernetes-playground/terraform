variable "aws_region" {
  type = "string"
}
variable "digital_ocean_token" {
  type = "string"
}
variable "do_region" {
  type = "string"
}
variable "namespace" {
  type = "string"
}
variable "do_database_size" {
  type = "string"
  default = "db-s-1vcpu-1gb"
}
