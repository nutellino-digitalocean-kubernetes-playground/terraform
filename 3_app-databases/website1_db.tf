variable "website1_password" {
  default = "w1password"
}

resource "mysql_user" "website1" {
  user               = "website1"
  host               = "%"
  plaintext_password = "${var.website1_password}"
}

resource "mysql_database" "website1" {
  name = "website1"
}

resource "mysql_grant" "website1" {
  user       = "${mysql_user.website1.user}"
  host       = "${mysql_user.website1.host}"
  database   = "${mysql_database.website1.name}"
  privileges = ["ALL"]
}


resource "null_resource" "website1_change_native" {
  depends_on = ["mysql_user.website1"]
  triggers {
    password = "${var.website1_password}"
    fix = "${var.fix}"
  }
  provisioner "local-exec" {
    command = "sleep 30 && echo \"ALTER USER ${mysql_user.website1.user} IDENTIFIED WITH mysql_native_password BY '${var.website1_password}';\" | mysql -u ${data.terraform_remote_state.database.database_username} -p${data.terraform_remote_state.database.database_password} -h ${data.terraform_remote_state.database.database_endpoint} -P ${data.terraform_remote_state.database.database_port}"
  }
}