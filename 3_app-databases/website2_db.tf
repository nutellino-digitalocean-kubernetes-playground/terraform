variable "website2_password" {
  default = "w2password"
}

resource "mysql_user" "website2" {
  user               = "website2"
  host               = "%"
  plaintext_password = "${var.website2_password}"
}

resource "mysql_database" "website2" {
  name = "website2"
}

resource "mysql_grant" "website2" {
  user       = "${mysql_user.website2.user}"
  host       = "${mysql_user.website2.host}"
  database   = "${mysql_database.website2.name}"
  privileges = ["ALL"]
}


resource "null_resource" "website2_change_native" {
  depends_on = ["mysql_user.website2"]
  triggers {
    password = "${var.website2_password}"
    fix = "${var.fix}"
  }
  provisioner "local-exec" {
    command = "sleep 30 && echo \"ALTER USER ${mysql_user.website2.user} IDENTIFIED WITH mysql_native_password BY '${var.website2_password}';\" | mysql -u ${data.terraform_remote_state.database.database_username} -p${data.terraform_remote_state.database.database_password} -h ${data.terraform_remote_state.database.database_endpoint} -P ${data.terraform_remote_state.database.database_port}"
  }
}