provider "aws" {
  region = "${var.aws_region}"
}

provider "mysql" {
  endpoint = "${data.terraform_remote_state.database.database_endpoint}:${data.terraform_remote_state.database.database_port}"
  username = "${data.terraform_remote_state.database.database_username}"
  password = "${data.terraform_remote_state.database.database_password}"
}
