output "website1_user" {
  value = "${mysql_user.website1.user}"
}

output "website1_password" {
  value = "${var.website1_password}"
}

output "website1_endpoint" {
  value = "${data.terraform_remote_state.database.database_endpoint}:${data.terraform_remote_state.database.database_port}"
}

output "website1_host" {
  value = "${data.terraform_remote_state.database.database_endpoint}"
}


output "website1_port" {
  value = "${data.terraform_remote_state.database.database_port}"
}


output "website2_user" {
  value = "${mysql_user.website2.user}"
}

output "website2_password" {
  value = "${var.website2_password}"
}

output "website2_endpoint" {
  value = "${data.terraform_remote_state.database.database_endpoint}:${data.terraform_remote_state.database.database_port}"
}

output "website2_host" {
  value = "${data.terraform_remote_state.database.database_endpoint}"
}


output "website2_port" {
  value = "${data.terraform_remote_state.database.database_port}"
}