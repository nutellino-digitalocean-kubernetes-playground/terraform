//stable/wordpress

resource "helm_release" "website2" {
  name      = "website2"
  chart     = "stable/wordpress"
  namespace = "website2"
  version = "7.6.7"

  timeout = 600

  set {
    name  = "wordpressScheme"
    value = "https"
  }
  set {
    name  = "mariadb.enabled"
    value = "false"
  }
  set {
    name  = "externalDatabase.host"
    value = "${data.terraform_remote_state.app-databases.website2_host}"
  }
  set {
    name  = "externalDatabase.user"
    value = "${data.terraform_remote_state.app-databases.website2_user}"
  }
  set_sensitive {
    name  = "externalDatabase.password"
    value = "${data.terraform_remote_state.app-databases.website2_password}"
  }
  set {
    name  = "externalDatabase.database"
    value = "website2"
  }
  set {
    name  = "externalDatabase.port"
    value = "${data.terraform_remote_state.app-databases.website2_port}"
  }
  set {
    name  = "service.type"
    value = "ClusterIP"
  }
  set {
    name  = "ingress.enabled"
    value = "true"
  }
  set {
    name  = "ingress.certManager"
    value = "true"
  }
  set {
    name  = "ingress.hostname"
    value = "website2.tuttorotto.it"
  }


  set {
    name  = "ingress.hosts[0].name"
    value = "website2.tuttorotto.it"
  }


  //ingress.tls[0].hosts[0]
  set {
    name  = "ingress.tls[0].hosts[0]"
    value = "website2.tuttorotto.it"
  }
  set {
    name  = "ingress.tls[0].secretName"
    value = "wordpress.local-tls"
  }
  set {
    name  = "persistence.enabled"
    value = "true"
  }
  set {
    name  = "persistence.storageClass"
    value = "nfs"
  }
  set {
    name  = "persistence.accessMode"
    value = "ReadWriteMany"
  }

  set {
    name = "livenessProbeHeaders[0].name"
    value = "X-Forwarded-Proto"
  }
  set {
    name = "livenessProbeHeaders[0].value"
    value = "https"
  }

  set {
    name = "readinessProbeHeaders[0].name"
    value = "X-Forwarded-Proto"
  }
  set {
    name = "readinessProbeHeaders[0].value"
    value = "https"
  }

  set {
    name = "wordpressUsername"
    value = "ugorossi"
  }

  set {
    name = "wordpressEmail"
    value = "ugo@rossi.it"
  }
  set {
    name = "wordpressFirstName"
    value = "Ugo"
  }
  set {
    name = "wordpressLastName"
    value = "Rossi"
  }
  set {
    name = "wordpressBlogName"
    value = "Best SECOND MEGA Blog Ever!"
  }
  set {
    name = "wordpressPassword"
    value = "ugo4ever"
  }

}