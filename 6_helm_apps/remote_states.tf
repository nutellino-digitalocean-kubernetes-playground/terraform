data "terraform_remote_state" "app-databases" {
  backend = "s3"
  config {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/app-databases/tfstate"
    region = "eu-west-1"
  }
}