resource "aws_route53_record" "website1-A"{
  zone_id = "${data.aws_route53_zone.zone.id}"
  name    = "website1.${var.domain}"
  type    = "A"
  records = ["${local.loadbalancer_ip}"]
  ttl     = "300"
}
