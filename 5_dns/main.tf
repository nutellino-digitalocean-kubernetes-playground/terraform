provider "aws" {
  region = "${var.aws_region}"
}


provider "kubernetes" {
  config_path = "${var.kubeconfig_path}"
}


data "aws_route53_zone" "zone" {
  name = "${var.domain}"
}

locals {
  loadbalancer_ip = "${data.kubernetes_service.external_ingress.load_balancer_ingress.0.ip}"
}