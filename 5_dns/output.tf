output "loadbalancer_ip" {
  value = "${data.kubernetes_service.external_ingress.load_balancer_ingress.0.ip}"
}