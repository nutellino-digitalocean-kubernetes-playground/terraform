resource "aws_route53_record" "website2-A"{
  zone_id = "${data.aws_route53_zone.zone.id}"
  name    = "website2.${var.domain}"
  type    = "A"
  records = ["${local.loadbalancer_ip}"]
  ttl     = "300"
}
