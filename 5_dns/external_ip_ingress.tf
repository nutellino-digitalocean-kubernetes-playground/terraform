data "kubernetes_service" "external_ingress" {
  metadata {
    name = "nginx-ingress-controller"
    namespace = "kube-system"
  }
}