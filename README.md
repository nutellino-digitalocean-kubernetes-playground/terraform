# Example Kubernetes on DO + Helm + AWS DNS + Mysql dbs provision + Mysql user provision +  Wordpress provision

DigitalOcean reference Infrastructure. Divided infrastructure in folders to be managed by runatlantis.

Tested on 11/11/2019

Prerequisites:

* `terraform` (v0.11) installed
* `kubectl` installed
* `helm` installed
* `mysql` command line installed
* `doctl` command line installed (to query DO api)
* S3 Bucket on AWS to save terraform remote states (is needed to access remote states from other projects to use outputs as "variables")

## Structure

* `1_kubernetes`: Creates a kubernetes cluster on DigitalOcean with autoscaling enabled, and pre-installed csi-s3 driver, custom cert manager crd and prerequisites for helm. Also doks-metrics-server to enable hpa autoscaling.
* `2_database`: Creates a mysql database cluster on digitalocean
* `3_app-databases`: Creates users and databases to be consumed by apps
* `4_helm_system`: deploys all system helm charts that will be required to deploy a fully functional app on k8s
* `5_dns`: Dns projects that query Kubernetes nginx-ingress load balancer ip and configures DNS records on AWS
* `6_helm_apps`: Helm project to manage all apps that will be deployed on the cluster

## Creating infrastructure

To create infrastructure you need to follow the order of the folders.
Create inside each folder your tfvars file to customize your installation.

Then launch `terraform apply`

## Destroying infrastructure

Follow inverse creation order to `terraform destroy` evertyhing. Check on cloud.digitalocean if all resources are deleted. 

## How to deploy

Next i will demostrate how to deploy each subproject. 

You need to have aws cli configured on your current shell session. See https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

You need also digital ocean project token. To pass explicit token on the provider, export in current shell the variable:

```bash
export TF_VAR_digital_ocean_token="123123123123123123123123"
```

You can create your token via:

![do_menu](resources/do_menu.png)


### 1_kubernetes

This project deals with the creation of the kubernetes cluster on digital ocean. 
In addition, the following system prerequisites are installed:

* Helm namespaces
* cert-manager CRDs
* doks-metrics-server (digitalocean metrics server to enable hpa autoscaling)

#### Configuration

Create a `terraform_backend.tf` file based on example, eg:

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/kubernetes/tfstate"
    region = "eu-west-1"
  }
}
```

* `terraform.nutellino-playground` is my S3 Bucket
* `terraform-do/kubernetes/tfstate` is terraform state s3 file
* `eu-west-1` is ireland aws region (where the bucket is)

Then you can configure these variables (create your name.auto.tfvars file) :

##### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | AWS Region configuration | string | - | yes |
| kubeconfig_path | Where will be stored kubeconfig file on current computer for example: `~/.kube/configdigitalocean` | string | - | yes |
| digital_ocean_token | Digitalocean token, automatic value form env `TF_VAR_digital_ocean_token` | string | - | yes |
| do_region | Digital Ocean region where k8s cluster will be provisioned, eg: `fra1` | string | - | yes |
| do_k8s_version | k8s digital ocean version, this infrastructure is tested against `1.14.8-do.1` | string | `1.14.8-do.1` | no |
| do_k8s_node_size | Worker node size | string | `s-1vcpu-2gb` | no |
| do_k8s_min_node | Worker node pool min size | string | `1` | no |
| do_k8s_max_node | Worker node pool max size | string | `3` | no |
| do_k8s_auto_scale | Worker node pool auto scaling | string | `true` | no |
| namespace | Resource prefix (name to prepend on created resource on do) | string | - | yes |



#### Launch creation

Now, to create the cluster you need to launch terraform apply. 

```bash
terraform init
terraform apply
```

After applying terraform you will find your cluster on digital ocean console, also you will have your kubeconfig file saved on the given directory

![after_kubernetes](resources/after_k8s.png)

### 2_database

In this project, a mysql cluster database will be created on digital ocean.

Outputs will be exported to be consumed on next projects.

#### Configuration

Create a `terraform_backend.tf` file based on example, eg:

**MANDATORY** change key on terraform backend

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/database/tfstate"
    region = "eu-west-1"
  }
}
```

* `terraform.nutellino-playground` is my S3 Bucket
* `terraform-do/database/tfstate` is terraform state s3 file
* `eu-west-1` is ireland aws region (where the bucket is)

Then you can configure these variables (create your name.auto.tfvars file) :

##### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | AWS Region configuration | string | - | yes |
| digital_ocean_token | Digitalocean token, automatic value form env `TF_VAR_digital_ocean_token` | string | - | yes |
| do_region | Digital Ocean region where k8s cluster will be provisioned, eg: `fra1` | string | - | yes |
| do_database_size | k8s digital database size eg: `db-s-1vcpu-1gb` | string | `db-s-1vcpu-1gb` | no |
| namespace | Resource prefix (name to prepend on created resource on do) | string | - | yes |


#### Launch creation

Now, to create the database cluster you need to launch terraform apply. 

```bash
terraform init
terraform apply
```

##### Outputs

| Name | Description |
|------|-------------|
| database_endpoint | mysql database hostname |
| database_username | mysql database username |
| database_password | mysql database password |
| database_port | mysql database port |

In digital ocean console you will find your databse:

![after_database](resources/after_database.png)

### 3_app-database

In this project we will configure users and database on previously created database cluster

Create a `terraform_backend.tf` file based on example, eg:

**MANDATORY** change key on terraform backend

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/app-databases/tfstate"
    region = "eu-west-1"
  }
}
```

* `terraform.nutellino-playground` is my S3 Bucket
* `terraform-do/app-databases/tfstate` is terraform state s3 file
* `eu-west-1` is ireland aws region (where the bucket is)

Create also a `remote_states.tf` file with reference to 2_database terraform state

```hcl-terraform
data "terraform_remote_state" "database" {
  backend = "s3"
  config {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/database/tfstate"
    region = "eu-west-1"
  }
}
```



This is an example with plain hardcoded password, in real world you need to use something like git-crypt to encrypt secrets.

Then you can configure these variables (create your name.auto.tfvars file) :

##### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | AWS Region configuration | string | - | yes |
| digital_ocean_token | Digitalocean token, automatic value form env `TF_VAR_digital_ocean_token` | string | - | yes |


#### Launch creation

Now, to users and databases with terraform: 

```bash
terraform init
terraform apply
```

In this example two users and two database will be created

##### Outputs

| Name | Description |
|------|-------------|
| website1_user | mysql database user1 |
| website1_password | mysql database password for user1 |
| website1_endpoint | host:port |
| website1_host | mysql database host |
| website1_port | mysql database port |
| website2_user | mysql database user2 |
| website2_password | mysql database password for user2 |
| website2_endpoint | host:port |
| website2_host | mysql database host |
| website2_port | mysql database port |

In digital ocean console you can check your newly created user and passwords

![after_db_users](resources/after_db_users.png)


### 4_helm_system

Now the good part begin. We will deploy all system services needed for a fully functional cluster via helm.

We will deploy:

* cert-manager to automatically manage let's encrypt certificates
* two cluster issuer based on custom chart https://gitlab.com/nutellino-digitalocean-kubernetes-playground/charts/tree/master/charts/cert-issuers
* nginx-ingress controller that will automatically deploy a digital ocean **loadbalancer**
* nfs-provisioner to automatically create nfs mountable pvc inside k8s cluster

As usual, create a `terraform_backend.tf` file based on example, eg:
          
**MANDATORY** change key on terraform backend

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/helm-system/tfstate"
    region = "eu-west-1"
  }
}
```

* `terraform.nutellino-playground` is my S3 Bucket
* `terraform-do/helm-system/tfstate` is terraform state s3 file
* `eu-west-1` is ireland aws region (where the bucket is)

As usual, you can configure variables (create your name.auto.tfvars file) :

##### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | AWS Region configuration | string | - | yes |
| digital_ocean_token | Digitalocean token, automatic value form env `TF_VAR_digital_ocean_token` | string | - | yes |
| email_cert_issuer | Email used for let's encrypt comunications | string | - | yes |


#### Launch creation

Deploy all chart with: 

```bash
terraform init
terraform apply
```

You will need to apply two times, because sometimes apiserver will throw `unable to handle request`

```bash
* helm_release.nfs-provisioner: rpc error: code = Unknown desc = Could not get apiVersions from Kubernetes: unable to retrieve the complete list of server APIs: webhook.cert-manager.io/v1beta1: the server is currently unable to handle the request
```

A second `terraform apply` will fix

we will now check in digital ocean console two things:

first we will find a newly created load balancer:

![after_loadbalancer](resources/after_loadbalancer.png)

and then, a pvc attached to our nfs-provisioner

![after_nfs](resources/after_nfs.png)

and that's it! We can now start to add real application inside k8s.

### 5_dns

In this example, we will retrieve from kubernetes the external ip of our loadbalancer. Then with this ip we will create two record A on our domain:

* website1.`<domain>`
* website2.`<domain>`

Where domain is your var configuration.

Create a `terraform_backend.tf` file based on example, eg:

**MANDATORY** change key on terraform backend

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/dns/tfstate"
    region = "eu-west-1"
  }
}
```

* `terraform.nutellino-playground` is my S3 Bucket
* `terraform-do/dns/tfstate` is terraform state s3 file
* `eu-west-1` is ireland aws region (where the bucket is)



As usual, you can configure variables (create your name.auto.tfvars file) :

##### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | AWS Region configuration | string | - | yes |
| domain | Your domain where records will be added (in my case `tuttorotto.it` | string | - | yes |
| kubeconfig_path | Your kubeconfig path | string | - | yes |

#### Launch creation

Deploy dns records with: 

```bash
terraform init
terraform apply
```

With this step our two dns records will be pointed toward our new loadbalancer

Wait 1-2 minutes for propagation and go to next step


### 6_helm_apps

In this last project, we will create two wordpress webiste backed by dynamic nfs pvc (data will be stored inside nfs-provisioner pvc), database on digitalocean managed mysql cluster and SSL certificate emitted by cert-manager


Create a `terraform_backend.tf` file based on example, eg:

**MANDATORY** change key on terraform backend

```hcl-terraform
terraform {
  backend "s3" {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/helm-apps/tfstate"
    region = "eu-west-1"
  }
}
```

* `terraform.nutellino-playground` is my S3 Bucket
* `terraform-do/helm-apps/tfstate` is terraform state s3 file
* `eu-west-1` is ireland aws region (where the bucket is)

Create also a `remote_states.tf` file with reference to app-databases terraform state (to retrieve users, passwords, endpoints and so on)

```hcl-terraform
data "terraform_remote_state" "app-databases" {
  backend = "s3"
  config {
    bucket = "terraform.nutellino-playground"
    key    = "terraform-do/app-databases/tfstate"
    region = "eu-west-1"
  }
}
```

As usual, you can configure variables (create your name.auto.tfvars file) :

##### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | AWS Region configuration | string | - | yes |
| kubeconfig_path | Your kubeconfig path | string | - | yes |

#### Launch creation

Deploy wordpress websites with: 

```bash
terraform init
terraform apply
```

This deploy will also trigger a cluster autoscale from 1->2 because our single node (1vcpu and 2Gb ram) is not enough for two wordpress deployments.

We will now check if everything is completed successfully:

![after_helm](resources/after_helm.gif)