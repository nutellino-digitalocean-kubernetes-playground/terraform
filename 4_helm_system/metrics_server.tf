resource "helm_release" "metrics_server" {
  name = "metrics-server"
  chart = "stable/metrics-server"
  version = "2.8.8"
  namespace = "kube-system"

  set {
    name  = "args[0]"
    value = "--metric-resolution=10s"
  }
  set {
    name  = "args[1]"
    value = "--kubelet-preferred-address-types=InternalIP"
  }
}

