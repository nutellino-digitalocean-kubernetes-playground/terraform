variable "region" {
  type = "string"
}
variable "kubeconfig_path" {
  type = "string"
}
variable "email_cert_issuer" {
  type = "string"
}