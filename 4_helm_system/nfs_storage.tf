resource "helm_release" "nfs-provisioner" {
  name      = "nfs-provisioner"
  chart     = "stable/nfs-server-provisioner"
  namespace = "kube-system"
  version = "0.3.1"
  set {
    name  = "persistence.enabled"
    value = "true"
  }

  //persistence.size
  set {
    name  = "persistence.size"
    value = "50Gi"
  }

}