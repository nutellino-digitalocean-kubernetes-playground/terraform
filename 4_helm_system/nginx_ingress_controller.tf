resource "helm_release" "nginx-ingress" {
  depends_on = ["helm_release.cert-manager"]
  name      = "nginx-ingress"
  chart     = "stable/nginx-ingress"
  namespace = "kube-system"
  version = "1.24.7"

  set {
    name  = "controller.publishService.enabled"
    value = "true"
  }

}