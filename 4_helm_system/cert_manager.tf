data "helm_repository" "jetstack" {
  name = "jetstack"
  url  = "https://charts.jetstack.io"
}

resource "helm_release" "cert-manager" {
  name      = "cert-manager"
  chart     = "jetstack/cert-manager"
  namespace = "kube-system"
  version = "0.11.0"
  set {
    name  = "ingressShim.defaultIssuerKind"
    value = "ClusterIssuer"
  }
  set {
    name  = "ingressShim.defaultIssuerName"
    value = "letsencrypt-production"
  }

}

resource "helm_release" "cert-issuers" {
  depends_on = ["helm_release.cert-manager"]
  name      = "cert-issuers"
  chart     = "https://nutellino-digitalocean-kubernetes-playground.gitlab.io/charts/cert-issuers-0.0.1.tgz"
  namespace = "kube-system"
  set {
    name  = "email"
    value = "${var.email_cert_issuer}"
  }


}