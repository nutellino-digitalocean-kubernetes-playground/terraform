resource "digitalocean_kubernetes_cluster" "kubernetes_cluster" {
  name    = "${var.namespace}-1"
  region  = "${var.do_region}"
  // Grab the latest version slug from `doctl kubernetes options versions`
  version = "${var.do_k8s_version}" #1.14.8-do.1

  node_pool {
    name       = "${var.namespace}-pool"
    size       = "${var.do_k8s_node_size}" #s-1vcpu-2gb
    min_nodes = "${var.do_k8s_min_node}"   #1
    max_nodes = "${var.do_k8s_max_node}"   #3
    auto_scale = "${var.do_k8s_auto_scale}" #true
  }
}