variable "aws_region" {
  type = "string"
}
variable "kubeconfig_path" {
  type = "string"
}
variable "digital_ocean_token" {
  type = "string"
}
variable "do_region" {
  type = "string"
}
variable "do_k8s_version" {
  type = "string"
  default = "1.14.8-do.1"
}
variable "do_k8s_node_size" {
  type = "string"
  default = "s-1vcpu-2gb"
}
variable "do_k8s_min_node" {
  type = "string"
  default = "1"
}
variable "do_k8s_max_node" {
  type = "string"
  default = "3"
}
variable "do_k8s_auto_scale" {
  type = "string"
  default = "true"
}
variable "namespace" {
  type = "string"
}