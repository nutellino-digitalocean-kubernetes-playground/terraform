resource "null_resource" "setup_k8s" {
  depends_on = ["digitalocean_kubernetes_cluster.kubernetes_cluster"]
  triggers {
    kubeconfig = "${digitalocean_kubernetes_cluster.kubernetes_cluster.kube_config.0.raw_config}"
  }

  provisioner "local-exec" {
    command = "mkdir -p ~/.kube/ && echo \"${digitalocean_kubernetes_cluster.kubernetes_cluster.kube_config.0.raw_config}\" > ${var.kubeconfig_path}"
  }
  provisioner "local-exec" {
    command = "KUBECONFIG=${var.kubeconfig_path} kubectl apply -f ${path.module}/system_deployments/helm/ || true"
  }

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.kubeconfig_path} kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml --validate=false || true"
  }

}